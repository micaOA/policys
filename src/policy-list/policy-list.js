import { LitElement, html } from 'lit-element';
import '@polymer/iron-collapse/iron-collapse.js';

class PolicyList extends LitElement {

    static get properties() {
        return {
            name: {type: String},
            policy:{type:String},
            dateAdd:{type:String},
            dateVig:{type:String}
        };
    }

    constructor() {
        super();
        this.name="";
        this.policy="";
        this.dateAdd="";
        this.dateVig="";
    }

    render() {
        return html`
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">   
        <div class="container">
        <div class="row align-items-start">
       
            <div class="col ">
            <p class="card-text">${this.name}</p>
            </div>
            <div class="col">
            <p class="card-text">${this.policy}</p>
            </div>
            <div class="col">
            <p class="card-text">${this.dateAdd}</p>
            </div>
            <div class="col">
            <p class="card-text">${this.dateVig}</p>
            </div>
            
            <div class="col">
                <a id="trigger" class="btn btn-danger"  @click="${this.deleteClient}">Delete</a>
            </div>
            <div class="col">
                <a id="trigger" class="btn btn-primary" data-bs-toggle="collapse"  role="button" aria-expanded="false" aria-controls="collapse" @click="${this.toggle}">Details</a>
                
                <iron-collapse id="collapse" aria-hidden="false" class="iron-collapse-opened">
                    <div class="content">
                            <policy-details>
                            
                            </policy-details>  
                    </div>
                </iron-collapse>
            </div>
        </div>
        </div>`;
            
    }
    toggle() {
        var n = this.shadowRoot.getElementById('collapse');
        console.log("toggle",n)
        var isHidden = n.getAttribute('aria-hidden')
  
        if(isHidden == "true"){
          n.setAttribute('aria-hidden', "false")
          n.classList.add("iron-collapse-opened")
          n.classList.remove("iron-collapse-closed")
        } else {
          n.setAttribute('aria-hidden', "true")
          n.classList.add("iron-collapse-closed")
          n.classList.remove("iron-collapse-opened")
        }
      }

      deleteClient(e){
        console.log("Eliminar");
        var id=e.detail.id;
        let xhr=new XMLHttpRequest();
       
            xhr.onload=function(){
                console.log("petición exitosa");
            }.bind(this);
            xhr.open("DELETE", "http://localhost:8080/alumnos/eliminar/"+id);
            xhr.send();
    }
       
}

customElements.define('policy-list', PolicyList)