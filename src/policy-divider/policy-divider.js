
import {LitElement, html, css} from 'lit-element';

class PolicyDivider extends LitElement{
    
	static get styles()
    {
            return css `

                     .break{
            height:20px;
                     }
            
                     .bd-toc-item.active>.bd-toc-link {
                        color: rgba(0,0,0,.85);
                    }         `;
    }

render(){
   return html`
   <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    
   <form class="bd-search d-flex align-items-center ">    
   <ul class="slide-nav nav flex-column">
            <li class="nav-item">
                <a class="nav-link active" aria-current="page" href="/" >Home</a>
            </li>
            <li class="active">
                <a class="nav-link active" aria-current="page" href="/main/lista">Consulta polizas</a>
            </li>
            <li class="active">
                <a class="nav-link active" aria-current="page" href="/main/actualiza">Actualiza datos</a>
            </li>
            <li class="active">
                <a class="nav-link active" aria-current="page" href="/main/alta">Alta de clientes</a>
            </li>
        </ul>
    </form> 
   
    
    `;
}


}
customElements.define('policy-divider',PolicyDivider)