import {LitElement, html} from 'lit-element';
import '../policy-details/policy-details'
import '../policy-list/policy-list'



class PolicyMain extends LitElement{
  static get properties(){
    return{
      clientes:{type:Array},
    }
  }
    constructor(){
        super();
        this.clientes=[];
        this.getData();
        this.showPersonForm=false;
        
    }

    render(){
        return html`
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous"> 
        <div class="container">            
        <div class="row align-items-start">
                      
                      <div class="col card-header ">
                        <h5 class="card-title">Nombre cliente</h5>
                      </div>
                      <div class="col card-header">
                        <h5 class="card-title">Poliza</h5>
                      </div>
                      <div class="col card-header">
                        <h5 class="card-title">FechaAlta</h5>
                      </div>
                      <div class="col card-header">
                        <h5 class="card-title">Fecha-Vigencia</h5>
                      </div>
                      <div class="col card-header">
                        <h5 class="card-title">...</h5>
                      </div>
                      <div class="col card-header">
                      <h5 class="card-title">..</h5>
                      </div>
                    
                    </div>
                     
                    ${this.clientes.map(
                      cliente => html`<policy-list 
                                name=${cliente.nombre} 
                                policy=${cliente.telefono} 
                                dateAdd=${cliente.direccion} 
                                dateVig=${cliente.numero_poliza}></policy-list>`
                      
                      
                  )}
            
        
           


        </div>
            
        
        `;
    }
   
 

      getData(){
        console.log("getClients");
        let xhr=new XMLHttpRequest();
       
            xhr.onload=function(){
                if(xhr.status === 200){
                    console.log("petición exitosa");
                    let APIResponse =JSON.parse(xhr.responseText);
                    this.clientes=APIResponse;
                    console.log(this.clientes);
                }
            }.bind(this);
            xhr.open("GET", "http://localhost:9999/apihack/v2/clientes");
            xhr.send();
    }


}
customElements.define('policy-main', PolicyMain);