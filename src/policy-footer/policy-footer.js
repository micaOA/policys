import {LitElement, html, css} from 'lit-element'
class PolicyFooter extends LitElement{
    static get styles()
    {
            return css `
            .footer {
                position: absolute;
                bottom: 0;
                width: 100%;
                height: 60px;
                background-color: #0660cb;
            }
            .navbar-hackaton {
                background-color: #072146;
               
              }
              .content-separation {
                margin-top: 15px;
              }
              .fill-button {
                width: 100%;
              }
              .action-icon {
                font-size: 50px;
              }
              p{
          
                color: #f8f8f8;
              }`;
     }
render(){
    return html`
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <div class="footer navbar-hackaton"><p>@Copyright Team 1</p></div>
    `;
}

}
customElements.define('policy-footer',PolicyFooter)