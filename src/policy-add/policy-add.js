import {LitElement, html, css} from 'lit-element'
class PolicyAdd extends LitElement{

static get Styles(){
    return css `
    .header {
        color: #36A0FF;
        font-size: 27px;
        padding: 10px;
    }
    
    .bigicon {
        font-size: 35px;
        color: #36A0FF;
    }
    .navbar-hackaton {
        background-color: #072146;
       
      }
      .content-separation {
        margin-top: 15px;
      }
      .fill-button {
        width: 100%;
      }
      .action-icon {
        font-size: 50px;
      }
      p{
  
        color: #f8f8f8;
      }
    `;
}
    render(){
        return html`
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="well well-sm">
                <form class="form-horizontal" method="post">
                    <fieldset>
                        <legend class="text-center header">Alta de cliente</legend>

                        <div class="form-group">
                            <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-user bigicon"></i></span>
                            <div class="col-md-8">
                                <input id="fname" name="name" type="text" placeholder="Nombre" class="form-control" @input="${this.updateName}" >
                            </div>
                        </div>
                        <div class="form-group">
                            <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-user bigicon"></i></span>
                            <div class="col-md-8">
                                <input id="lname" name="name" type="text" placeholder="Apellidos" class="form-control" @input="${this.updateName}" >
                            </div>
                        </div>

                        <div class="form-group">
                            <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-envelope-o bigicon"></i></span>
                            <div class="col-md-8">
                                <input id="email" name="email" type="text" placeholder="Dirección" class="form-control" @input="${this.updateName}" >
                            </div>
                        </div>

                        <div class="form-group">
                            <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-phone-square bigicon"></i></span>
                            <div class="col-md-8">
                                <input id="phone" name="phone" type="text" placeholder="Telefono" class="form-control" @input="${this.updateName}" >
                            </div>
                        </div>

                        <div class="form-group">
                            <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-pencil-square-o bigicon"></i></span>
                            <div class="col-md-8">
                                <textarea class="form-control" id="message" name="message" placeholder="Enter your massage for us here. We will get back to you within 2 business days." rows="7" @input="${this.updateName}" ></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12 text-center">
                                <button type="submit" class="btn btn-primary btn-lg" @click=${this.newClient}>Guardar</button>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</div>
        
        `;
    }

    newClient(e){
        console.log("newPerson en persona-sidebar");
	    console.log("Se va a crear una nueva persona");
  
	    this.dispatchEvent(new CustomEvent("new-client", {})); 
    }
    updateName(e) {
        console.log("Name");
        console.log("Ingresa Name " + e.target.value);
        this.person.name = e.target.value;
    }
    updateProfile(e) {
		console.log("Poliza");
		console.log("Ingresa Poliza " + e.target.value);
		this.person.profile = e.target.value;
    }
    updateProfile(e) {
		console.log("telefono");
		console.log("Ingresa telefono" + e.target.value);
		this.person.profile = e.target.value;
    }
    updateYearsInCompany(e) {
		console.log("Direcciòn");
		console.log("Ingresa direcciòn" + e.target.value);
		this.person.yearsInCompany = e.target.value;
    }
    storePerson(e) {
        console.log("storePerson");
        e.preventDefault();
        
        this.person.photo = {
            "src": "./img/persona.jpg",
            "alt": "Persona"
        };
            
        console.log("La propiedad name vale " + this.person.name);
        console.log("La propiedad profile vale " + this.person.profile);
        console.log("La propiedad yearsInCompany vale " + this.person.yearsInCompany);	
            
        this.dispatchEvent(new CustomEvent("persona-form-store",{
            detail: {
                person:  {
                        name: this.person.name,
                        profile: this.person.profile,
                        yearsInCompany: this.person.yearsInCompany,
                        photo: this.person.photo
                    }
                }
            })
        );
        console.log("guardar");
        let xhr=new XMLHttpRequest();
       
            xhr.onload=function(){
                console.log("petición exitosa");
            }.bind(this);
            xhr.open("POST", "http://localhost:8080/alumnos/crear");
            xhr.send(this.person.name);
    }
    guardar(){
        console.log("guardar");
        let xhr=new XMLHttpRequest();
       
            xhr.onload=function(){
                console.log("petición exitosa");
            }.bind(this);
            xhr.open("POST", "http://localhost:8080/alumnos/crear");
            xhr.send(this.person.name);
    }

}
customElements.define('policy-add',PolicyAdd);
