import {LitElement, html, css} from 'lit-element'
class PolicyHeader extends LitElement{
     static get styles()
    {
            return css `
     
        .navbar-hackaton {
          background-color: #072146;
         
        }
        .content-separation {
          margin-top: 15px;
        }
        .fill-button {
          width: 100%;
        }
        .action-icon {
          font-size: 50px;
        }
        h6{
          
          color: #f8f8f8;
        }
        a{
          
          color: #f8f8f8;
        }`;
     }

render(){
   return html`
   <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
   <header class=" navbar-hackaton navbar navbar-expand navbar-dark flex-column flex-md-row bd-navbar" ">
     <a class="navbar-brand col-md-3 col-lg-2 me-0 px-3" href="#">BBVA</a>
     <a class="navbar-brand col-md-3 col-lg-2 me-0 px-3" href="#"><strong>GESTIÓN POLIZAS</strong></a>
     <form class="d-flex">
      <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
      <button class="btn btn-outline-success" type="submit">Search</button>
    </form>
     <ul class="navbar-nav">
          <li class="nav-item text-nowrap">
          <a class="nav-link" href="#">Salir</a>
          </li>
     </ul>
   </header>
    `;
}

}
customElements.define('policy-header',PolicyHeader);