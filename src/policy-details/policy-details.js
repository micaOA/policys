import { LitElement, html} from 'lit-element';
class PolicyDetails extends LitElement{

    static get properties(){
        return{
            idClient:{type:Number},
            phone:{type:String},
            addres:{type:String},
          
        };
    }

    constructor(){
        super();
        this.idClient=0;
        this.phone=0;
        this.addres='';
        
    }

    render(){
        return html`
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">                <div class=class="card w-75">
                     <div class="card-body">
                        <h3 class="card-title">Sección Datos Asegurado</h3>
                        <div class="form-group">
                            <label><strong>Id de Cliente</strong></label>
                            <label>${this.idClient}</label>
                        </div>
                        <div class="form-group">
                            <label><strong>Teléfono</strong></label>
                            <label>${this.phone}</label>
                        </div>
                        <div class="form-group">
                            <label><strong>Dirección</strong></label>
                            <label>${this.addres}</label>
                        </div>
                     <div>

                     <div>
                      <h3 class="card-title">Sección Datos de Seguro<h3>
                     <div>
                </div>
        
        `;
    }

 

}


customElements.define('policy-details', PolicyDetails);
