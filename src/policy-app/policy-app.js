import { LitElement, html } from 'lit-element';
import '../policy-header/policy-header';
import '../policy-footer/policy-footer';
import '../policy-divider/policy-divider';



class PolicyApp extends LitElement {

    static get properties() {
        return {
        };
    }

    constructor() {
        super();
    }

    render() {
        return html`
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">        
        
        <div class="container-fluid">
         
        <div class="row flex-xl-nowrap">      
        <policy-divider class="col-12 col-md-3 col-xl-2 bd-sidebar"></policy-divider>
        <div class="col-12 col-md-9 col-xl-8 py-md-3 pl-md-5 bd-content">
                <slot></slot>
        </div>
        </div>
        </div>
                
       
        
        `;
    }
   
    
    
    
    
}

customElements.define('policy-app', PolicyApp)


