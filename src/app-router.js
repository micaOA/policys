
import { Router } from '@vaadin/router';



window.addEventListener('load', () => { 
  initRouter();
});
function initRouter() {
    const router = new Router(document.querySelector('app-router')); 
    router.setRoutes([
      {
        path: '/',
        component: 'policy-main',
        action: () =>
        import('./policy-main/policy-main') //
      },
      {
        path: '/main/lista',
        component: 'policy-main',
        action: () =>
          import('./policy-main/policy-main') // 
      },
      {
          path: '/main/actualiza',
          component: 'policy-details',
          action: () =>
            import('./policy-details/policy-details') // 
        },
        {
          path: '/main/alta',
          component: 'policy-add',
          action: () =>
            import('./policy-add/policy-add') // 
        },
      
    ]);
  }